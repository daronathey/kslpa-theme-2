<?php

$context = Timber::get_context();

$archive_id = get_option('page_for_lpac_meeting');
$meetings = new Timber\PostQuery();

$context['next_meeting'] = false;

$next = Timber::get_posts([
    'post_type' => 'lpac_meeting',
    'post_status' => ['future'],
    'order' => 'ASC',
    'posts_per_page' => 1
]);

if ($next){
    $context['next_meeting'] = $next[0];
}

/**
 * Arrange meetings into years
 * Example: $posts[2019]['meeting name here']
 */
$sorted_meetings = [];
foreach ($meetings as $meeting){
    $year = intval($meeting->date('Y'));
    if ( ! isset($sorted_meetings[$year]) ) $sorted_meetings[$year] = [];

    $sorted_meetings[$year][] = $meeting;
}

$context['posts'] = $meetings;

$post = new TimberPost($archive_id);
$context['post'] = $post;

Timber::render( 'archive-meeting.twig', $context );
