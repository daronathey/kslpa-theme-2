import Datepicker from 'vuejs-datepicker';
import format from 'date-fns/format';

/* global Vue, _, jQuery */

Vue.component('modal', {
    props: ['show'],
    template: `
        <transition name="modal">
            <div class="pattern-modal">
                <div class="pattern-modal__bg" @click="close"></div>
                <div class="pattern-modal__content">
                    <slot></slot>
                    <button class="pattern-modal__close-button button--plain" @click="close" aria-label="Close"><span aria-hidden="true">X</span></button>
                </div>
            </div>
        </transition>
    `,
    methods: {
        close: function(){
            this.$emit('close');
        }
    },
    mounted: function () {
        document.addEventListener("keydown", (e) => {
            if (this.show && e.keyCode == 27) {
                this.close();
            }
        });
    }
});

Vue.component('single-unit', {
    template: '#single-unit',
    props: ['unit'],
    data: function(){
        return {

        }
    },
    methods: {
        getUnitDimensions: function(unit){
            var dimensions = '';

            dimensions += unit.width + "' x " + unit.depth + "'";

            if ( unit.height ){
                dimensions += " x " + unit.height + "'";
            }
            
            return dimensions;
        },
        addToCart: function(unit){
            // item, type
            window.app.addToCart(unit, 'units');
            window.location = '/checkout';
        }
    }
});

Vue.component('unit-nav', {
    template: '#unit-nav',
    props: ['location', 'type'],
    data: function(){
        return {
            allUnits: window.cwssUnitMatrix,
            currentSize: ''
        }
    },
    methods:{
        changeSize: function(size){
            this.currentSize = size;
        },
        getUnitsFromSize: function(size){
            return _.filter(this.units, obj => {
                if ( (obj.size === "Covered" && size === "RV / Boat / Trailer") || (obj.size === "Uncovered" && size === "RV / Boat / Trailer") ){
                    return true;
                } else {
                    return obj.size === size;
                }
            });
        },
        getUnitDimensions: function(unit){
            var dimensions = '';

            dimensions += unit.width + "' x " + unit.depth + "'";

            if ( unit.height ){
                dimensions += " x " + unit.height + "'";
            }
            
            return dimensions;
        }
    },
    computed: {
        units: function(){
            var units;
            // only filter location and type if present on component
            units = this.location ? _.filter(this.allUnits, obj => obj.location.post_title === this.location) : this.allUnits;
            units = this.type ? _.filter(units, obj => obj.type === this.type) : units;

            return units;
        },
        unitSizes: function(){
            let sizes = [];

            this.units.map(unit => {
                sizes.push(unit.size);
            });

            sizes = _.uniq(sizes);

            // Small, Medium, Large, Extra Large happen to be in reverse alphabetical order
            // We'll remove un/covered below if necessary
            sizes.sort().reverse();

            let coveredIndex = sizes.indexOf('Covered');
            let uncoveredIndex = sizes.indexOf('Uncovered');

            if ( this.type !== 'RV/Boat/Trailer' ){
                if (coveredIndex > -1){
                    sizes.splice(coveredIndex, 1);
                }
                
                if (uncoveredIndex > -1) {
                    sizes.splice(uncoveredIndex, 1);
                }

                if (uncoveredIndex > -1 || coveredIndex > -1){
                    sizes.push('RV / Boat / Trailer');
                }
            }

            return sizes;
        }
    },
    mounted: function(){
        this.currentSize = this.unitSizes[0];
    }
});

window.app = new Vue({ // eslint-disable-line no-unused-vars
    el: '#vue-wrapper',
    components: {
        Datepicker
    },
    data: {
        loading: false,
        unitMatrix: window.cwssUnitMatrix,
        trailers: window.cwssTrailers,
        supplies: window.cwssMovingSupplies,
        locationUnitTypePages: window.cwssLocationUnitTypePages,
        showMoveInBreakdown: false,
        showRecurringBreakdown: false,
        showChooseUnitModal: false,
        showChooseUnitModal2: false,
        showReviewModal: false,
        showSuppliesDetailsModal: false,
        suppliesDetails: {},
        chooseUnitModalUnits: [],
        chooseUnitModalType: "",
        chooseUnitModalLocation: "",
        chooseUnitModalSize: {},
        home: false,
        currentTab: 'Small',
        disabledDates: {
            to: new Date()
        },
        checkoutComplete: false,
        checkout: {
            submitting: false,
            step: 1,
            first_name: '',
            last_name: '',
            phone: '',
            email: '',
            address: '',
            address2: '',
            city: '',
            state: '',
            zip: '',
            moveIn: '',
            insurance: "",
            extras: [],
            cart: {
                units: [],
                supplies: [],
                trailers: []
            },
            errors: []
        }
    },
    methods: {
        changeTab: function( size ){
            this.currentTab = size;
        },
        formatDate: function(date){
            return format(date, 'MMM D, YYYY');
        },
        populateHomeUnitModal: function(type){
            this.showChooseUnitModal = true;
            this.home = true;
            this.chooseUnitModalUnits = this.unitMatrix;
            this.chooseUnitModalType = type;
        },
        populateHomeUnitModal2: function(type){
            this.showChooseUnitModal2 = true;
            this.home = true;
            this.chooseUnitModalUnits = this.unitMatrix;
            this.chooseUnitModalType = type;
        },
        populateChooseUnitModal: function(collection){
            this.showChooseUnitModal = true;
            this.chooseUnitModalUnits = collection.units;
            this.chooseUnitModalSize = {width: collection.w, depth: collection.d, height: collection.h}
            var uniqueTypes = this.getUniqueTypesFromCollection(collection);
            var uniqueLocations = this.getUniqueLocationsFromCollection(collection);

            if ( uniqueTypes.length === 1){
                this.chooseUnitModalType = uniqueTypes[0];
            }

            if ( uniqueLocations.length === 1){
                this.chooseUnitModalLocation = uniqueLocations[0];
            }
        },
        clearChooseUnitModal: function(){
            this.showChooseUnitModal = false;
            this.showChooseUnitModal2 = false;
            this.chooseUnitModalType = "";
            this.chooseUnitModalLocation = "";
            this.chooseUnitModalSize = {};
        },
        clearReviewModal: function(){
            this.showReviewModal = false;
        },
        redirectFromChooseUnitModal: function(){
            // wp-json/wp/v2/posts?filter[meta_key]=MY-KEY&filter[meta_value]=MY-VALUE
            this.loading = true;
            var location = this.chooseUnitModalLocation.post_title;
            var type = this.chooseUnitModalType;
            
            var destination = _.find(this.locationUnitTypePages, obj => {
                if (obj.location === location && obj.unit_type === type){
                    return true;
                }
            });
            
            if ( destination ){
                window.location = destination.guid;
            } else {
                this.loading = false;
                alert('Something went wrong. Please try again later.');
            }
        },
        addUnitFromChooseUnitModal: function(){
            this.loading = true;
            var location = this.chooseUnitModalLocation;
            var type = this.chooseUnitModalType;
            var size = this.chooseUnitModalSize;

            var unit = _.find(this.unitMatrix, obj => {
                if (
                    obj.location.post_title === location && 
                    obj.type === type && 
                    obj.width === size.width && 
                    obj.height === size.height && 
                    obj.depth === size.depth
                ){
                    return true;
                }
            });

            if (!unit){
                this.loading = false;
                alert('Something went wrong. Please try again later.');
            }

            this.addToCart(unit, 'units');
            window.location = '/checkout';
        },
        getUnitDimensions: function(unit){
            var dimensions = '';

            dimensions += unit.width + "' x " + unit.depth + "'";

            if ( unit.height ){
                dimensions += " x " + unit.height + "'";
            }
            
            return dimensions;
        },
        getUniqueSizeUnits: function(size){
            var collection = _.filter(this.unitDimensions, obj => obj.size === size );
            
            if ( size === 'RV' ){
                var covered = _.filter(this.unitDimensions, obj => obj.size === 'Covered' );
                var uncovered = _.filter(this.unitDimensions, obj => obj.size === 'Uncovered' );
                collection = collection.concat(covered, uncovered);
            }
            
            return collection;
        },
        getUniqueTypesFromCollection: function( collection ){
            var types = [];

            var units = collection.units || collection;

            units.map(unit => {
                types.push(unit.type);
            });
            types = _.uniq(types);

            return types;
        },
        getUniqueLocationsFromCollection: function( collection ){
            var locations = [];
            
            var units = collection.units || collection;

            units.map(unit => {
                locations.push(unit.location.post_title);
            });
            locations = _.uniq(locations);

            return locations;
        },
        getUniqueLocationsFromCollectionAndType: function( collection, type ){
            var locations = [];
            
            var units = collection.units || collection;

            units.map(unit => {
                if (unit.type === type){
                    locations.push(unit.location);
                    // console.log(unit.location.popup_features);
                }
            });
            locations = _.uniq(locations, false, obj => obj.ID);

            return locations;
        },
        getCollectionDimensions: function(collection){
            var dimensions = '';

            dimensions += collection.w + "' x " + collection.d + "'";

            if ( collection.h ){
                dimensions += " x " + collection.h + "'";
            }
            
            return dimensions;
        },
        formatPrice: function(price){
            if (price === 0){
                return "FREE";
            }
            if ( isNaN(parseFloat(price)) ){
                return "N/A";
            }
            return price ? "$" + parseFloat(price).toFixed(2) : "N/A";
        },
        checkoutNavMove: function(direction){
            if ( direction === undefined ){
                direction = 1;
            }

            if (direction === -1){
                this.checkout.step = this.checkout.step - 1;
                return;
            }

            var errors = this.checkForErrors();
            
            if (!errors){
                this.checkout.step = this.checkout.step + 1;
            }
        },
        trailerIsInCart: function(id){
            id = parseInt(id);
            let trailer = _.find(this.checkout.cart.trailers, obj => obj.ID === id );

            if (trailer){
                return true;
            }
            return false;
        },
        addTrailerToCartById: function(id){
            id = parseInt(id);
            let trailer = _.find(this.trailers, obj => obj.ID === id );
            this.addToCart(trailer, 'trailers');
        },
        suppliesIsInCart: function(id){
            id = parseInt(id);
            let supplies = _.find(this.checkout.cart.supplies, obj => obj.ID === id );

            if (supplies){
                return true;
            }
            return false;
        },
        addSuppliesToCartById: function(id){
            id = parseInt(id);
            let supplies = _.find(this.supplies, obj => obj.ID === id );
            this.addToCart(supplies, 'supplies');
        },
        addToCart: function(item, type){
            this.checkout.cart[type].push(item);
        },
        removeUnit: function(unit){
            // var redirect;

            // if ( this.checkout.cart.units.length === 1 ){
            //     redirect = confirm('Are you sure you want to remove this unit? Removing this unit will redirect you to the homepage.\n\nClick OK to remove the unit and be redirected, otherwise click CANCEL.');
            //     if ( ! redirect ){
            //         return;
            //     }
            // }
            this.checkout.cart.units.splice(this.checkout.cart.units.indexOf(unit), 1);
            // if ( redirect ){
            //     window.location = "/";
            // }
        },
        showSuppliesDetails: function(supplies){
            this.suppliesDetails = supplies;
            this.showSuppliesDetailsModal = true;
        },
        areSuppliesInCart: function(supplies){
            var inCart = _.find(this.checkout.cart.supplies, obj => obj.ID === supplies.ID);
            return inCart;
        },
        isTrailerInCart: function(trailer){
            var inCart = _.find(this.checkout.cart.trailers, obj => obj.ID === trailer.ID);
            return inCart;
        },
        removeSupplies: function(supplies){
            console.log('removed supplies');
            this.checkout.cart.supplies.splice(this.checkout.cart.units.indexOf(supplies), 1);
        },
        removeTrailer: function(trailer){
            this.checkout.cart.trailers.splice(this.checkout.cart.units.indexOf(trailer), 1);
        },
        processReservation: function(){
            var errors = this.checkForErrors();
            
            if (errors){
                return;
            }

            this.checkout.submitting = true;

            //set variables
			var route = "forms/1/submissions";
			var url = '/gravityformsapi/' + route;

			var values = {
				input_values : {
					"input_1": this.checkout.first_name + ' ' + this.checkout.last_name,
                    "input_2_1": this.checkout.address,
                    "input_2_2": this.checkout.address2,
					"input_2_3": this.checkout.city,
					"input_2_5": this.checkout.zip,
					"input_3": this.checkout.phone,
					"input_4": this.checkout.email,
					"input_5": this.formatDate(this.checkout.moveIn),
					"input_6": this.formatUnitsForSubmission(this.checkout.cart.units),
					"input_7": this.formatSuppliesForSubmission(this.checkout.cart.supplies),
                    "input_8": this.formatTrailersForSubmission(this.checkout.cart.trailers),
                    "input_9": `${this.checkout.insurance.plan} - ${this.formatPrice(this.checkout.insurance.price)}`,
                    "input_10": this.totalMoveInPrice,
                    "input_11": this.totalRecurringPrice
                },
                _gf_json_nonce: window.cwssGfAPI
			};
			//json encode array
			var values_json = JSON.stringify(values);
			var self = this;
			jQuery.post(url, values_json, function(data){
                console.log(data);
			    if ( data.response.is_valid ){
                    self.checkout.submitting = false;
                    self.checkoutComplete = true;
                    self.checkout.step = 4;
                    window.localStorage.clear();
                    jQuery('#cart-count').text(0);
			    } else {
                    self.checkout.errors.push('Error submitting request. Please call or try again later.');
                    self.checkout.submitting = false;
                }
			});

        },
        formatUnitsForSubmission: function(units){
            let string = "\n";
            var that = this;
            
            _.forEach(units, unit => {
                var special = unit.special ? " - Special" : "";
                string += `${that.getUnitDimensions(unit)} - ${unit.type} - ${unit.location.post_title} - ${that.formatPrice(unit.price)}${special}\n`;
            });

            return string;
        },
        formatSuppliesForSubmission: function(items){
            let string = "\n";
            var that = this;
            
            _.forEach(items, item => {
                string += `${item.post_title} - ${that.formatPrice(item.price)}\n`;
            });

            return string;
        },
        formatTrailersForSubmission: function(items){
            let string = "\n";
            var that = this;
            
            _.forEach(items, (item, index) => {
                // if we have units, first trailer is free
                var price = (index === 0 && that.checkout.cart.units.length) ? 0 : item.price;
                string += `${item.post_title} - ${that.formatPrice(price)}\n`;
            });

            return string;
        },
        scrollToTop: function(){
            var element = jQuery('#checkout');
            var offset = element.offset().top;
            if(!element.is(":visible")) {
                element.css({"visibility":"hidden"}).show();
                offset = element.offset().top;
                element.css({"visibility":"", "display":""});
            }

            var visible_area_start = jQuery(window).scrollTop();
            var visible_area_end = visible_area_start + window.innerHeight;

            if(offset < visible_area_start || offset > visible_area_end){
                // Not in view so scroll to it
                jQuery('html,body').animate({scrollTop: offset - window.innerHeight/3}, 1000);
                return false;
            }
            return true;
        },
        checkForErrors: function(){
            this.checkout.errors = [];

            var errors = [];

            if (this.checkout.step === 1){
                if (!this.checkout.first_name.length){
                    errors.push('First Name is Required');
                }
                if (!this.checkout.last_name.length){
                    errors.push('Last Name is Required');
                }
                if (!this.checkout.phone.length){
                    errors.push('Phone Number is Required');
                }
                if (!this.checkout.email.length){
                    errors.push('Email Address is Required');
                }
                if (!this.checkout.address.length){
                    errors.push('Street Address is Required');
                }
                if (!this.checkout.city.length){
                    errors.push('City is Required');
                }
                if (!this.checkout.state.length){
                    errors.push('State is Required');
                }
                if (!this.checkout.zip.length){
                    errors.push('Zip is Required');
                }
            }

            if (this.checkout.step === 2){
                if (!this.checkout.moveIn && this.checkout.cart.units.length > 0){
                    errors.push('Reservation Date is Required');
                }
                if (!this.checkout.insurance && this.checkout.cart.units.length){
                    errors.push('An Insurance Option is Required');
                }
            }

            if (this.checkout.step === 3){
                if (!this.checkout.cart.supplies.length){
                    errors.push('Please choose a box option')
                }

                if (!this.checkout.cart.trailers.length){
                    errors.push('Please choose a trailer option')
                }
                
                if (!this.checkout.moveIn){
                    errors.push('Reservation Date is Required');
                }
            }

            this.checkout.errors = errors;

            return errors.length;
        }
    },
    computed: {
        numberOfCartUnits: function(){
            var plural = this.checkout.cart.units.length === 1 ? 'Unit' : 'Units';
            return `${this.checkout.cart.units.length} ${plural}`;
        },
        numberOfTrailers: function(){
            var plural = this.checkout.cart.trailers.length === 1 ? 'Trailer' : 'Trailers';
            return `${this.checkout.cart.trailers.length} ${plural}`;
        },
        firstMonthRentalPrice: function(){
            var total = 0;
            
            this.checkout.cart.units.forEach(unit => {
                total += unit.special ? 25.00 : parseFloat(unit.price);
            });

            return this.formatPrice(total);
        },
        totalDepositAmount: function(){
            return this.formatPrice(this.checkout.cart.units.length * 20);
        },
        totalTrailerDepositAmount: function(){
            var sum = 0;
            this.checkout.cart.trailers.forEach(trailer => {
                if ( trailer.price ){
                    sum += 50;
                }
            });
            return this.formatPrice(sum);
        },
        totalMoveInPrice: function(){
            var total = 0;
            
            this.checkout.cart.units.forEach(unit => {
                total += unit.special ? 25.00 : parseFloat(unit.price);
            });
            
            // unit deposit
            total += parseFloat(this.checkout.cart.units.length * 20);
            
            this.checkout.cart.supplies.forEach(s => {
                total += parseFloat(s.price);
            });

            this.checkout.cart.trailers.forEach((trailer, index) => {
                // first trailer rental is free if renting a unit
                if (index > 0){
                    total += parseFloat(trailer.price);
                } else if (!this.checkout.cart.units.length) {
                    total += parseFloat(trailer.price);
                }
            });
            
            // trailer deposit
            this.checkout.cart.trailers.forEach(trailer => {
                if (trailer.price > 0){
                    total += parseFloat(50)
                }
            })

            if ( this.checkout.insurance.price > 0 ){
                total += parseFloat(this.checkout.insurance.price);
            }

            return this.formatPrice(total);
        },
        totalRecurringPrice: function(){
            var total = 0;
            this.checkout.cart.units.forEach(unit => {
                total += parseFloat(unit.price);
            });
            if ( ! isNaN(parseFloat(this.checkout.insurance.price)) && this.checkout.insurance.price > 0 ){
                total += parseFloat(this.checkout.insurance.price);
            }
            return this.formatPrice(total);
        },
        totalRecurringUnitPrice: function(){
            var total = 0;
            this.checkout.cart.units.forEach(unit => {
                total += parseFloat(unit.price);
            });
            return this.formatPrice(total);
        },
        checkoutNavButtonText: function(){
            return this.checkout.step === 1 ? "Continue" : "Back";
        },
        checkoutNavMoveDirection: function(){
            return this.checkout.step === 1 ? 1 : -1;  
        },
        unitSizes: function(){
            var sizes = {};
            
            this.unitMatrix.forEach(unit => {
                sizes[unit.size] = [];
            });

            this.unitMatrix.forEach(unit => {
                sizes[unit.size].push(unit);
            });

            return sizes;
        },
        unitDimensions: function(){
            var dimensions = {};

            this.unitMatrix.forEach(unit => {
                var w = unit.width;
                var d = unit.depth;
                var h = unit.height;
                var t = w + d + h;

                dimensions[t] = {
                    w: w,
                    d: d,
                    h: h,
                    size: unit.size,
                    units: []
                };

            });

            this.unitMatrix.forEach(unit => {
                var w = unit.width;
                var d = unit.depth;
                var h = unit.height;
                var t = w + d + h;

                dimensions[t].units.push(unit);
            });

            return dimensions;
        },
        itemsInCart: function(){
            var count = 0;
            _.each(this.checkout.cart, value => {
                count += value.length;
            });

            return count;
        },
        showTrailerDeposit: function(){
            var sum = 0;
            this.checkout.cart.trailers.forEach(trailer => {
                sum += trailer.price;
            });
            console.log(sum);
            return this.checkout.cart.trailers.length && sum;
        }
    },
    watch: {
        'checkout.cart': {
            deep: true,
            handler: function(val){
                window.localStorage.setItem('cart', JSON.stringify(val));
                var date = new Date();
                date = date.setHours(date.getHours()+2);
                window.localStorage.setItem('cartExpires', date);
                jQuery('#cart-count').text(this.itemsInCart);
            }
        },
        chooseUnitModalLocation: function(val){
            if (val.length && !this.home){
                // this.redirectFromChooseUnitModal();
                this.addUnitFromChooseUnitModal();
            } else {
                this.redirectFromChooseUnitModal();
            }
        },
        chooseUnitModalType: function(val){
            var locations = this.getUniqueLocationsFromCollectionAndType(this.chooseUnitModalUnits, val);
            if (locations.length === 1){
                this.chooseUnitModalLocation = locations[0];
            }
        },
        'checkout.step': function(){
            this.scrollToTop();
        }
    },
    mounted: function(){
        var cart = JSON.parse(window.localStorage.getItem('cart'));
        var date = Date.now();
        
        if ( parseInt(window.localStorage.getItem('cartExpires')) < date ){
            cart = null;
        }

        if ( cart === null ){
            window.localStorage.setItem('cart', JSON.stringify(this.checkout.cart));
        } else {
            this.checkout.cart = cart;
        }
        
        // sort trailers with most expensive first
        this.checkout.cart.trailers = _.sortBy(this.checkout.cart.trailers, 'price').reverse();
    }
});