
/* jshint devel: true */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position){
			position = position || 0;
			return this.substr(position, searchString.length) === searchString;
	};
}

/**
 * FitVids JS
 */
;(function( $ ){

	'use strict';

	$.fn.fitVids = function( options ) {
		var settings = {
			customSelector: null,
			ignore: null
		};

		if(!document.getElementById('fit-vids-style')) {
			// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
			var head = document.head || document.getElementsByTagName('head')[0];
			var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
			var div = document.createElement("div");
			div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
			head.appendChild(div.childNodes[1]);
		}

		if ( options ) {
			$.extend( settings, options );
		}

		return this.each(function(){
			var selectors = [
				'iframe[src*="player.vimeo.com"]',
				'iframe[src*="youtube.com"]',
				'iframe[src*="youtube-nocookie.com"]',
				'iframe[src*="kickstarter.com"][src*="video.html"]',
				'object',
				'embed'
			];

			if (settings.customSelector) {
				selectors.push(settings.customSelector);
			}

			var ignoreList = '.fitvidsignore';

			if(settings.ignore) {
				ignoreList = ignoreList + ', ' + settings.ignore;
			}

			var $allVideos = $(this).find(selectors.join(','));
			$allVideos = $allVideos.not('object object'); // SwfObj conflict patch
			$allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

			$allVideos.each(function(){
				var $this = $(this);
				if($this.parents(ignoreList).length > 0) {
					return; // Disable FitVids on this video.
				}
				if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
				if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
				{
					$this.attr('height', 9);
					$this.attr('width', 16);
				}
				var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
						width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
						aspectRatio = height / width;
				if(!$this.attr('name')){
					var videoName = 'fitvid' + $.fn.fitVids._count;
					$this.attr('name', videoName);
					$.fn.fitVids._count++;
				}
				$this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
				$this.removeAttr('height').removeAttr('width');
			});
		});
	};
	
	// Internal counter for unique video names.
	$.fn.fitVids._count = 0;
	
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */


var freezeVp = function(e) {
    e.preventDefault();
};

window.stopBodyScrolling = function(bool) {
    if (bool === true) {
        document.body.addEventListener("touchmove", freezeVp, false);
    } else {
        document.body.removeEventListener("touchmove", freezeVp, false);
    }
};


jQuery( document ).ready( function( $ ) {
	var $body = $('body');
	$body.fitVids();

	$('#menu-toggle').click(function(){
		$('#mobile-nav').slideToggle();
	});

	$('#footer .gform_wrapper').removeClass('gform_wrapper').addClass('relative');

	// $('.open-popup-link').magnificPopup({
	// 	type:'inline',
	// 	removalDelay: 300,
	// 	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	// });
	
	// $('.magnific-trigger').magnificPopup({
	// 	type:'inline',
	// 	removalDelay: 300,
	// 	midClick: true
	// });

	$('body').on( 'click', '.open-audit-abstract', function(e){
		e.preventDefault();

		var $abstract = $(this).next('.audit-abstract').find('.audit-abstract-content');

		$.magnificPopup.open({
			items: {
				src: $abstract,
				type: 'inline'
			}
		});
		
	});

	$('.single-audit').find('.kslpa-starts-audit-preview-section').each(function(){
		var $sectionEls = $(this).nextUntil(".kslpa-starts-audit-preview-section").andSelf();
		$sectionEls.wrapAll('<div class="kslpa-audit-section">');
	});

	$('.kslpa-audit-section').prepend('<button type="button" class="toggle-section"><span class="arrow"></span><span class="sr-only">Toggle Section Visibility</span></button>');

	$('.single-audit').on('click', '.toggle-section', function(e){
		$(e.target).closest('.kslpa-audit-section').toggleClass('open');
	});

	$('.single-audit').on('click' ,'.expand-all-preview-sections', function(){
		$('.kslpa-audit-section').addClass('open');
	});

	$('.single-audit').on('click', '.collapse-all-preview-sections', function(){
		$('.kslpa-audit-section').removeClass('open');
	});

}); // End Document Ready

window.appendpassword = function( elId, event ){
	event.preventDefault();
	
	var el = document.getElementById(elId);

	var pass = el.value;
	
	// window.location = '#audituser=' + pass;
	history.pushState({}, '', window.location.href.split('?')[0] + '?audituser=' + pass);

	var form = document.getElementById('audit-password-form');

	form.submit();
	
	console.log(pass);
};

window.showViewableAudits = function(){
	if ( !window.FWP ) {
		return; 
	}

	if ( window.FWP.facets.only_viewable ){
		window.FWP.facets.only_viewable = ! window.FWP.facets.only_viewable;
	} else {
		window.FWP.facets.only_viewable = true;
	}
	window.FWP.set_hash();
	window.FWP.fetch_data();
};

jQuery(window).on('load', function(){
	if ( window.FWP && window.FWP.facets.only_viewable ){
		jQuery('#show-viewable-reports').check();
	}
});