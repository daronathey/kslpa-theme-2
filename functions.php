<?php
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class kslpaSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'disable-custom-colors' );
		add_theme_support('editor-styles');

		add_editor_style('editor-style.css');

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );

		add_action( 'init', array( $this, 'register_shortcake') );
		add_action( 'init', array( $this, 'add_acf_options_page' ) );

		add_action('acf/init', array( $this, 'register_blocks' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'dashboard_glance_items', array( $this, 'dashboard_glance_items' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
		add_filter( 'wp_nav_menu_objects', array( $this, 'wp_nav_menu_objects'), 10, 2);

		add_filter( 'gutenberg_can_edit_post_type', array( $this, 'disable_gutenberg'), 10, 2 );
		add_filter( 'use_block_editor_for_post_type', array( $this, 'disable_gutenberg'), 10, 2 );

		add_action('widgets_init', function(){
			// register_sidebar( array( 'name' => 'Blog Sidebar', 'id' => 'primary' ) );
		});

		add_filter( 'wp_insert_post_data', function( $data, $postarr ){
			if ( 'audit' == $data['post_type'] && 'auto-draft' == $data['post_status'] ) {
				$data['post_password'] = 'lpaauditpassword';
			}
			return $data;
		}, '99', 2 );

		add_action('pre_get_posts', function($query){
			if ($query->is_archive() && $query->query_vars['post_type'] === 'audit' && isset($_GET['fwp_only_viewable']) && $_GET['fwp_only_viewable']) {
				if ( function_exists('get_field') ){
					$term_id = get_field('it_audit_term', get_option('page_for_audit'));
					if ( $term_id ){
						$query->set('tax_query', [['taxonomy' => 'audit_subject', 'terms' => $term_id, 'operator' => 'NOT IN']]);
					}
				}
			}
		}, 10, 1);

		add_filter('acf/settings/show_admin', '__return_false');

		add_filter('posts_where', function($where){
			// Make sure this only applies to loops / feeds on the frontend
			if (!is_single() && !is_admin()) {
				// exclude password protected
				$where .= " AND post_password = ''";
			}
			return $where;
		});

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['unit_matrix'] = $context['options']['unit_matrix'];
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['primaryMenu'] = new TimberMenu('primary');
		$context['footerMenu'] = new TimberMenu('footer');
		$context['quickLinksMenu'] = new TimberMenu('quick_links');
		// $context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		
		return $context;
	}

	function after_setup_theme(){
		register_nav_menu( 'primary', 'Main Header Navigation' );
		register_nav_menu( 'footer', 'Footer Links' );
		register_nav_menu( 'quick_links', 'Quick Links' );
		// Images Sizes
		add_image_size( 'xlarge', 2880, 2000 );
	}

	function enqueue_scripts(){
		// Dependencies
		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120206' );
		wp_enqueue_style( 'kslpa-slick-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css', '20120206' );
		wp_enqueue_style( 'kslpa-slick-theme-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick-theme.css', '20120206' );
		
		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'kslpa-slick', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'kslpa-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore', 'magnific-popup' ), '20160910' );
		// wp_enqueue_script( 'kslpa-video', get_template_directory_uri() . "/static/js/container.player.min.js", array( 'jquery', 'underscore' ), '20160820', true );
		wp_enqueue_script( 'kslpa-vue', get_template_directory_uri() . "/static/js/vue.js", array( 'jquery', 'underscore' ), '20160822', true );
		
		wp_localize_script( 'magnific-popup', "wpThemeUrl", get_stylesheet_directory_uri() );
	}

	function tiny_mce_external_plugins( $plugin_array ) {
		$plugin_array['typekit']  =  $this->theme->link . '/static/js/typekit.tinymce.js';
		return $plugin_array;
	}

	function register_post_types(){
		include_once('inc/post-type-employee.php');
		include_once('inc/post-type-audit.php');
		include_once('inc/post-type-lpac_meeting.php');
		include_once('inc/post-type-upcoming_audit.php');
	}

	function register_shortcake(){
		include 'inc/shortcake.php';
	}

	function disable_gutenberg( $can_edit, $post_type ) {

		if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
			return $can_edit;
		

		if ( class_exists('Page_For_Post_Type') ){
			$pages = (Page_For_Post_Type::get_instance())->get_page_ids();
			if ( in_array( $_GET['post'], $pages ) ){
				$can_edit = false;
			}
		}
	
		return $can_edit;
	
	}

	function dashboard_glance_items( $items ){
		foreach ( get_post_types(array('public'=>true)) as $post_type ){
			$num_posts = wp_count_posts( $post_type );
			if ( $num_posts && $num_posts->publish ) {
				if ( 'post' == $post_type ) {
					continue;
				}
				if ( 'page' == $post_type ) {
					continue;
				}
				$post_type_object = get_post_type_object( $post_type );
				$text = _n( '%s ' . $post_type_object->labels->singular_name, '%s ' . $post_type_object->label, $num_posts->publish );
				$text = sprintf( $text, number_format_i18n( $num_posts->publish ) );
				if ( $post_type_object && current_user_can( $post_type_object->cap->edit_posts ) ) {
					$items[] = sprintf( '<a href="edit.php?post_type=%1$s">%2$s</a>', $post_type, $text );
				} else {
					$items[] = sprintf( '<span>%2$s</span>', $post_type, $text );
				}
			}
		}
		
		return $items;
	}

	function add_acf_options_page(){
		if ( ! function_exists('acf_add_options_page') ){
			return;
		}

		// acf_add_options_page(array(
		// 	'page_title' 	=> 'Site Options',
		// 	'menu_title'	=> 'Site Options',
		// 	'menu_slug' 	=> 'kslpa-site-options',
		// 	'capability'	=> 'edit_posts',
		// 	'redirect'		=> false
		// ));

	}

	function register_blocks(){
		 // check function exists.
		 if( function_exists('acf_register_block_type') ) {

			// Register a side-by-side block.
			acf_register_block_type(array(
				'name'              => 'sidebyside_ctas',
				'title'             => __('Side by Side CTAs'),
				'description'       => __('Side by side call to actions.'),
				'render_callback'   => array($this, 'sidebyside_cta'),
				'category'          => 'formatting',
				'mode'				=> 'preview'
			));

			acf_register_block_type(array(
				'name'              => 'career_benefits',
				'title'             => __('Career Benefits'),
				'description'       => __('3 benefits with a button link underneath.'),
				'render_callback'   => array($this, 'career_benefits'),
				'category'          => 'formatting',
				'mode'				=> 'edit'
			));

			acf_register_block_type(array(
				'name'              => 'career_gallery',
				'title'             => __('Career Gallery'),
				'description'       => __('A masonry layout gallery of images.'),
				'render_callback'   => array($this, 'career_gallery'),
				'category'          => 'formatting',
				'mode'				=> 'edit'
			));
		}
	}

	function sidebyside_cta( $block, $content = '', $is_preview, $post_id ){
		$context = Timber::get_context();
		$context['preview'] = $is_preview;

		$context['cta1_text'] = get_field('cta_1_title_text');
		$context['cta1_button'] = get_field('cta_1_button');

		$context['cta2_text'] = get_field('cta_2_title_text');
		$context['cta2_button'] = get_field('cta_2_button');
		

		Timber::render( 'block-sidebyside-cta.twig', $context );
	}

	function career_benefits( $block, $content = '', $is_preview, $post_id ){
		$context = Timber::get_context();
		$context['preview'] = $is_preview;

		$context['benefits'] = get_field('benefits');
		$context['more_benefits'] =  get_field('more_benefits');

		Timber::render( 'block-career-benefits.twig', $context );
	}

	function career_gallery( $block, $content = '', $is_preview, $post_id ){
		$context = Timber::get_context();
		$context['preview'] = $is_preview;

		$context['gallery'] = get_field('gallery');

		Timber::render( 'block-career-gallery.twig', $context );
	}

	function wp_nav_menu_objects( $items, $args ) {
		foreach( $items as &$item ) {
			$heading = get_field('heading', $item);
			// append icon
			if( $heading ) {
				$item->title = '<span class="menu-heading">' . $item->title . '</span>';	
			}
		}

		return $items;
	}

	function admin_head_css(){
		?>
			<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700&display=swap" rel="stylesheet">
		<?php
	}
}

new kslpaSite();

function kslpa_render_primary_menu(){ // used in base.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'primary-menu',
	) );
}

function kslpa_render_footer_menu(){ // used in base.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'footer',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'footer-menu',
	) );
}

function kslpa_is_recent_audit($audit){
	$month = date('n'); // 7
	
	$ago = strtotime('2 months ago');

	$audit_date = strtotime($audit->date('m/d/Y'));

	$today = time();

	$result =  $audit_date > $ago && $audit_date < $today;

	return $result;
}

/**
 * reindex after adding or updating this filter
 */
add_filter( 'facetwp_index_row', function( $params, $class ) {
	if ( 'audit_year' == $params['facet_name'] ) {
		$raw_value = $params['facet_value'];
		$params['facet_value'] = date( 'Y', strtotime( $raw_value ) );
		$params['facet_display_value'] = $params['facet_value'];
	}
	return $params;
}, 10, 2 );