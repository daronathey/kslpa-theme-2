<?php

$labels = array(
	'name'               => __( 'Audits', 'spha' ),
	'singular_name'      => __( 'Audit', 'spha' ),
	'add_new'            => _x( 'Add New Audit', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Audit', 'spha' ),
	'edit_item'          => __( 'Edit Audit', 'spha' ),
	'new_item'           => __( 'New Audit', 'spha' ),
	'view_item'          => __( 'View Audit', 'spha' ),
	'search_items'       => __( 'Search Audits', 'spha' ),
	'not_found'          => __( 'No Audits found', 'spha' ),
	'not_found_in_trash' => __( 'No Audits found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Audit:', 'spha' ),
	'menu_name'          => __( 'Audits', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array('audit_agency', 'audit_subject'),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-media-text',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'show_in_rest'        => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor'
	),
);

register_post_type( 'audit', $args );

register_taxonomy( 'audit_agency', 'audit', array(
	'labels' => array(
		'name'                       => _x( 'Agencies', 'Taxonomy General Name', 'kslpa' ),
		'singular_name'              => _x( 'Agency', 'Taxonomy Singular Name', 'kslpa' ),
		'menu_name'                  => __( 'Agencies', 'kslpa' ),
		'all_items'                  => __( 'All Items', 'kslpa' ),
		'parent_item'                => __( 'Parent Item', 'kslpa' ),
		'parent_item_colon'          => __( 'Parent Item:', 'kslpa' ),
		'new_item_name'              => __( 'New Item Name', 'kslpa' ),
		'add_new_item'               => __( 'Add New Item', 'kslpa' ),
		'edit_item'                  => __( 'Edit Item', 'kslpa' ),
		'update_item'                => __( 'Update Item', 'kslpa' ),
		'view_item'                  => __( 'View Item', 'kslpa' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'kslpa' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'kslpa' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'kslpa' ),
		'popular_items'              => __( 'Popular Items', 'kslpa' ),
		'search_items'               => __( 'Search Items', 'kslpa' ),
		'not_found'                  => __( 'Not Found', 'kslpa' ),
		'no_terms'                   => __( 'No items', 'kslpa' ),
		'items_list'                 => __( 'Items list', 'kslpa' ),
		'items_list_navigation'      => __( 'Items list navigation', 'kslpa' ),
	),
	'hierarchical' => true,
	'show_in_rest' => true
));


register_taxonomy( 'audit_subject', 'audit', array(
	'labels' => array(
		'name'                       => _x( 'Subjects', 'Taxonomy General Name', 'kslpa' ),
		'singular_name'              => _x( 'Subject', 'Taxonomy Singular Name', 'kslpa' ),
		'menu_name'                  => __( 'Subjects', 'kslpa' ),
		'all_items'                  => __( 'All Items', 'kslpa' ),
		'parent_item'                => __( 'Parent Item', 'kslpa' ),
		'parent_item_colon'          => __( 'Parent Item:', 'kslpa' ),
		'new_item_name'              => __( 'New Item Name', 'kslpa' ),
		'add_new_item'               => __( 'Add New Item', 'kslpa' ),
		'edit_item'                  => __( 'Edit Item', 'kslpa' ),
		'update_item'                => __( 'Update Item', 'kslpa' ),
		'view_item'                  => __( 'View Item', 'kslpa' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'kslpa' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'kslpa' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'kslpa' ),
		'popular_items'              => __( 'Popular Items', 'kslpa' ),
		'search_items'               => __( 'Search Items', 'kslpa' ),
		'not_found'                  => __( 'Not Found', 'kslpa' ),
		'no_terms'                   => __( 'No items', 'kslpa' ),
		'items_list'                 => __( 'Items list', 'kslpa' ),
		'items_list_navigation'      => __( 'Items list navigation', 'kslpa' ),
	),
	'hierarchical' => true,
	'show_in_rest' => true
));