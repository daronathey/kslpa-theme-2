<?php

$context = Timber::get_context();

$archive_id = get_option('page_for_audit');
$audits = Timber::get_posts();

/**
 * Arrange audits into years
 * Example: $posts[2019]['audit name here']
 */
$sorted_audits = [];
foreach ($audits as $audit){
    $year = intval($audit->date('Y'));
    if ( ! isset($sorted_audits[$year]) ) $sorted_audits[$year] = [];

    $sorted_audits[$year][] = $audit;
}

$context['posts'] = $audits;

$post = new TimberPost($archive_id);
$context['post'] = $post;

Timber::render( 'archive-audit.twig', $context );
