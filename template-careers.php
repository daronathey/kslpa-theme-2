<?php

/**
 * Template Name: Careers Page Template
 */

$context = Timber::get_context();

$context['post'] = Timber::get_post();

Timber::render( 'careers.twig', $context );
